package edu.uprm.cse.datastructures.problems;

import javax.naming.spi.DirStateFactory.Result;

import edu.uprm.cse.datastructures.set.ArraySet;
import edu.uprm.cse.datastructures.set.Set;

public class Disjoint {
	
	@SuppressWarnings("unchecked")
	public static boolean checkDisjoint (Object[] sets) {

		for(int i =1; i<sets.length; i++) {
			if(((Set<Integer>)sets[i]).intersection((Set<Integer>)sets[i-1]).isEmpty()) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		Set<Integer> S1 = new  ArraySet<Integer>();
		Set<Integer> S2 = new  ArraySet<Integer>();
		Set<Integer> S3 = new  ArraySet<Integer>();
		Set<Integer> S4 = new  ArraySet<Integer>();
		
		S1.add(1);
		S1.add(2);
		
		S2.add(0);
		
		S3.add(5);
		S3.add(8);
		S3.add(9);
		
		S4.add(5);

		@SuppressWarnings("unchecked")
		Object[] A1 =   new Object[3];
		A1[0] = S1;
		A1[1] = S2;
		A1[2] = S3;

		System.out.println("S1, S2, and S3 disjoint: " + checkDisjoint(A1));
		Object[] A2 =   new Object[4];
		A2[0] = S1;
		A2[1] = S2;
		A2[2] = S3;
		A2[3] = S4;

		System.out.println("S1, S2, S3, and S4 disjoint: " + checkDisjoint(A2));
		Set<Integer> S5 = new  ArraySet<Integer>();
		Set<Integer> S6 = new  ArraySet<Integer>();
		Set<Integer> S7 = new  ArraySet<Integer>();
		S5.add(1);
		S5.add(2);
		S6.add(2);
		S7.add(3);
		S7.add(2);

		Object[] A3 =   new Object[3];
		A3[0] = S5;
		A3[1] = S6;
		A3[2] = S7;
		System.out.println("S5, S6, and S7 disjoint: " + checkDisjoint(A3));

	}

}

