package edu.uprm.cse.datastructures.problems;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.uprm.cse.datastructures.bag.Bag;
import edu.uprm.cse.datastructures.bag.DynamicBag;
import edu.uprm.cse.datastructures.set.ArraySet;
import edu.uprm.cse.datastructures.set.Set;

public class Tester {

	@Test
	public void mostFrequentTest1() {
		Bag<Integer> B1 = new DynamicBag<Integer>();
		
		B1.add(1);
		B1.add(1);
		B1.add(2);
		B1.add(2);
		B1.add(2);
		B1.add(4);
		B1.add(3);
		B1.add(3);
		B1.add(3);
		B1.add(2);

		Bag<Integer> B2 = B1.mostFrequentThan(1);
		
		Object[] B3 = B2.toArray();
		
		for(Object i: B3) {
			if(((Integer) i) != 2 && ((Integer) i) != 3)
				fail("Wrong value was given. Expected answer is 2 and 3.");
		}
		assertTrue(B3.length == 2);
	}
	@Test
	public void mostFrequentTest2() {
		Bag<Integer> B1 = new DynamicBag<Integer>();
		
		B1.add(1);
		B1.add(1);
		B1.add(2);
		B1.add(2);
		B1.add(2);
		B1.add(4);
		B1.add(3);
		B1.add(3);
		B1.add(3);
		B1.add(2);

		Bag<Integer> B2 = B1.mostFrequentThan(2);
		
		assertTrue(B2.isEmpty());
	}
	@Test
	public void mostFrequentTest3() {
		Bag<String> B1 = new DynamicBag<String>();
		
		B1.add("Bob");
		B1.add("John");
		B1.add("Bob");
		B1.add("Mary");
		B1.add("Lex");
		B1.add("Bob");
		B1.add("Lex");

		Bag<String> B2 = B1.mostFrequentThan("Mary");
		
		Object[] B3 = B2.toArray();
		
		for(Object i: B3) {
			if(((String) i).equals("Bob") && ((String) i).equals("Lex"))
				fail("Wrong value was given. Expected answer is Bob and Lex.");
		}
		assertTrue(B3.length == 2);
	}
	@SuppressWarnings("unchecked")
	@Test
	public void singletonSetsTest1() {
		Set<String> S1 = new ArraySet<String>();
		
		S1.add("Kim");
		S1.add("Ned");
		S1.add("Ron");
		S1.add("Bon");

		Object[] singletonSets = S1.singletonSets();
		for(Object s: singletonSets) {
			if(!(s instanceof Set)) {
				fail("The Object[] that singletonSets() returns is supposed to be filled with Sets.");
			} 
			else {
				if(((Set<String>) s).size() != 1)
					fail("To be an array of singleton sets the sets must each have just one element.");
			}
		}
		assertTrue(singletonSets.length == 4);
	}

}

